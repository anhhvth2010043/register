<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Login - SB Admin</title>
    <link href="/css/styles.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
</head>
<body class="bg-primary">
<div id="layoutAuthentication">
    <div id="layoutAuthentication_content">
        <main>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="card shadow-lg border-0 rounded-lg mt-5">
                            <div class="card-header"><h3 class="text-center font-weight-light my-4">Update</h3></div>
                            <div class="card-body">
                                <form action="{{ $admin->id,route('admins.update') }}" method="POST">
                                    @method('PUT')
                                    @csrf
                                    <div class="form-floating mb-3">
                                        <input class="form-control" id="inputName" name="name" type="text" value="{{$admin->name}}"  />
                                        <label for="inputEmail">Name</label>
                                    </div>
                                    <div class="form-floating mb-3">
                                        <input class="form-control" id="inputPosition" name="position" type="text" value="{{$admin->position}}"  />
                                        <label for="inputPassword">Position</label>
                                    </div>
                                    <div class="form-floating mb-3">
                                        <input class="form-control" id="inputOfftice" name="offtice" type="text" value="{{$admin->offtice}}" />
                                        <label for="inputPassword">Offtice</label>
                                    </div>
                                    <div class="form-floating mb-3">
                                        <input class="form-control" id="inputAge" name="age" type="text" value="{{$admin->age}}" />
                                        <label for="inputPassword">Age</label>
                                    </div>
                                    <div class="form-floating mb-3">
                                        <input class="form-control" id="inputStart" name="start_date" type="text" value="{{$admin->start_date}}" />
                                        <label for="inputPassword">Start date</label>
                                    </div>
                                    <div class="form-floating mb-3">
                                        <input class="form-control" id="inputSalary"  name="salary" type="text" value="{{$admin->salary}}" />
                                        <label for="inputSalary">Salary</label>
                                    </div>
                                    <button type="submit" class="d-flex align-items-center btn btn-primary justify-content-between mt-4 mb-0">
                                        Create
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="/js/scripts.js"></script>
</body>
</html>
