<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    public function index()
    {
        $admins = Admin::latest()->paginate(5);
        return view('admins.index', compact('admins'));

    }

    public function create()
    {
        return view('admins.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'position' => 'required',
            'offtice' => 'required',
            'age'=>'required',
            'start_date'=>'required',
            'salary'=>'required'
        ]);
        Admin::create($request->all());
        return redirect()->route('admins.index')
            ->with('success', 'Product created successfully.');
    }

    public  function  edit(Admin $admin){
        return view('admins.edit',compact('admin'));
    }

    public function update(Request $request, Admin $admin){
        $request->validate([
            'name' => 'required',
            'position' => 'required',
            'offtice' => 'required',
            'age'=>'required',
            'start_date'=>'required',
            'salary'=>'required'
        ]);
        $admin->update($request->all());
        return redirect()->route('admins.index')
            ->with('success', 'Product created successfully.');
    }

    public function  destroy(Admin $admin){
        $admin->delete();
        return redirect()->route('admins.index')
            ->with('success', 'Product created successfully.');
    }
}
